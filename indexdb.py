'''
dbm(db meta) file format:
    terms-dict -- {term: posting_offset_in_dbi}    # a dict of terms
    docs-list  -- [doc]                            # a list of docs

dbi(db index) file format:
    posting -- [(docid, normalize_tf, [term_offset_in_doc])]    # a list of all docs which given term appears in
               ...repeat for all terms...

DB only load terms-dict and docs-dict from dbm, when doing search, DB load posting from dbi accroding to offset
in terms-dict.
'''

from collections import defaultdict
from functools import reduce
from tokenizer import process
from math import sqrt
import pickle

class DB(object):

    def __init__(self):
        self.docs = None
        self.terms = None


    # return a dict {term: (docid, normalized_tf, [term_offset_in_doc])}
    def _processDoc(doc, docid):
        #print("processing doc:", doc)
        with open(doc, 'r', encoding='utf-8') as f:
            try:
                tokens = process(f.read())
                terms = defaultdict(list)
                for token, offset in zip(tokens, range(len(tokens))):
                    terms[token].append(offset)
                if len(terms) > 0:
                    norm_fact = 1.0 / sqrt(sum([len(v)**2.0 for _, v in terms.items()]))
                    return {k: (docid, norm_fact*len(v), v) for k, v in terms.items()}
            except UnicodeDecodeError:
                None
            return {}


    # postings - {term: [(docid, normalized_tf, [term_offset_in_doc])]}
    # term_entry - the return value of _processDoc
    def _mergePostings(postings, term_entry):
        for k, v in term_entry.items():
            postings[k].append(v)
        return postings


    def _pintersect(offsets0, offsets1):
        ret = []
        i0, i1, e0, e1 = 0, 0, len(offsets0), len(offsets1)
        while i0 < e0 and i1 < e1:
            o0, o1 = offsets0[i0], offsets1[i1]
            if o0 + 1 == o1:
                ret.append(o1)
                i0 += 1
                i1 += 1
            elif o0 + 1 < o1:
                i0 += 1
            else:
                i1 += 1
        return ret


    def _intersect(p0, p1, positional=False):
        ret = []
        i0, i1, e0, e1 = 0, 0, len(p0), len(p1)
        while i0 < e0 and i1 < e1:
            d0, ntf0, offsets0 = p0[i0]
            d1, ntf1, offsets1 = p1[i1]
            if d0 == d1:
                if not positional:
                    ret.append(p0[i0])
                else:
                    offset = DB._pintersect(offsets0, offsets1)
                    if offset != []:
                        ret.append((d0, ntf0, offset))
                i0 += 1
                i1 += 1
            elif d0 < d1:
                i0 += 1
            else:
                i1 += 1
        return ret

    _DB_EXT = 'dbi'
    _DB_META_EXT = 'dbm'


    def _save(self, name):
        with open('%s.%s'%(name, DB._DB_EXT), 'wb') as f:
            terms_todisk = {}
            offset = 0
            for t in self.terms:
                d = pickle.dumps(self.terms[t])
                f.write(d)
                terms_todisk[t] = offset
                offset += len(d)
        with open('%s.%s'%(name, DB._DB_META_EXT), 'wb') as f:
            d = pickle.dump(terms_todisk, f)
            d = pickle.dump(self.docs, f)


    def _load_meta(self, name):
        with open('%s.%s'%(name, DB._DB_META_EXT), 'rb') as f:
            '''terms = {term:{docID:(normalize_tf, [term_offset_in_doc])}}'''
            self.terms = pickle.load(f)
            if not self.docs:
                self.docs = pickle.load(f)
        self.name = name


    def _build(self, docs, name):
        self.docs = list(docs)
        terms = reduce(DB._mergePostings, [DB._processDoc(doc, docid) for doc, docid in zip(docs,range(len(docs)))], defaultdict(list))
        self.terms = {k: sorted(v) for k, v in terms.items()}
        self._save(name)
        self._load_meta(name)


    def _load_posting(self, term):
        if term not in self.terms:
            return {}
        with open('%s.%s'%(self.name, DB._DB_EXT), 'rb') as f:
            offset = self.terms[term]
            f.seek(offset, 0)
            return pickle.load(f)


    '''
        @return
            - if scoring disable, return a list of docs as the result of multi words query or phrase query
            - if scoring enabled, return following structure: ([(doc, [term_normalized_tf])], [term_local_df])
    '''
    def search(self, terms, phrase=False, scoring=False):
        for t in terms:
            if t not in self.terms:
                if not scoring:
                    return []
                else:
                    return ([], [0])
        postings = [self._load_posting(t) for t in terms]
        p = postings[0]
        for pp in postings[1:]:
            p = DB._intersect(p, pp, phrase)
        try:
            docs = [self.docs[i] for i, _, _ in p]
        except:
            print(p)
        if not scoring:
            return docs
        else:
            tfs = [[tf for pp in postings for i, tf, _ in pp if i == docid] for docid, _, _ in p]
            local_dfs = [len(pp) for pp in postings]
            return (list(zip(docs, tfs)), local_dfs)


    def docs_count(self):
        return len(self.docs)


    def db_name(self):
        return self.name


    def create(docs_and_dbname):
        docs, dbname = docs_and_dbname
        db = DB()
        db._build(docs, dbname)
        return db


    def load(dbname):
        db = DB()
        db._load_meta(dbname)
        return db
