import re
import string


with open('config/patterns.txt', 'r', encoding='utf8') as f:
    plines = [ll for ll in [l.strip() for l in f] if ll[:2] != '//' and ll != '']
    patterns = [re.compile(l, re.IGNORECASE) for l in plines]


def is_allowed_ch(c):
    return c.isalpha() or (c >= '!' and c <= '~')


def split_by_punc(str):
    return ''.join([' ' if c in string.punctuation else c for c in str]).split()


def split_by_re(str):
    ret = []
    matched = False
    for p in patterns:
        m = p.search(str)
        if m != None:
            matched = True
            if m.start() > 0:
                ret += split_by_re(str[0:m.start()])
            ret.append(str[m.start() : m.end()])
            if m.end() < len(str):
                ret += split_by_re(str[m.end():])
            break
    return ret if matched else split_by_punc(str)


def process(raw_str):
    raw_tokens = ''.join([c if is_allowed_ch(c) else ' ' for c in raw_str.lower()]).split()
    tokens = []
    for t in raw_tokens:
        tokens += split_by_re(t)
    return tokens


def test():
    with open('testfile.txt', 'r', encoding='utf8') as infile:
        tokens = process(infile.read())
        with open('tokenized_testfile.txt', 'w', encoding='utf8') as outfile:
            outfile.write('\n'.join(tokens))


