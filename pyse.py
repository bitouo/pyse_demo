'''
Use following commands:
!exit                       - exit from the program
!add                        - add dataset
!rebuild                    - rebuild all loaded dataset
!list                       - list all loaded dataset
!help                       - show this message

Use follorwing search format:
term0 term1 ... termN       - search term0, term1 ... termN
"term0 term1 ... termN"     - phrase search term0, term1 ... terN
'''

import argparse, time
from shutil import rmtree
from os import listdir, mkdir
from os.path import isfile, isdir
from concurrent.futures import ProcessPoolExecutor as Executor, wait
from tokenizer import process
from functools import reduce
from indexdb import DB


def shorter_name(name):
    return name[name.rfind('/')+1:]


def list_all_files(dir):
    files = []
    for f in listdir(dir):
        f = dir+'/'+f
        if isfile(f):
            files.append(f)
        elif isdir(f):
            files += list_all_files(f)
    return files


def isCmd(cmdStr):
    return len(cmdStr) > 0 and cmdStr[0] == '!'


class Pyse(object):

    def __init__(self, num_processes, db_file_count, mt):
        self.num_processes = num_processes
        self.db_file_count = db_file_count
        self.mt_enabled = mt


    def start(self, datasets, force_rebuild):
        self.dbs = []
        self.datasets = []
        self.process_datasets(datasets, force_rebuild)
        self.mainloop()


    def process_datasets(self, datasets, force_rebuild):
        for ds in datasets:
            db_name = shorter_name(ds)
            db_path = '%s/%s_db'%('data', db_name)
            if not force_rebuild and isdir(db_path):
                print("load index db of %s..."%ds)
                self.dbs += self.load_db(db_path)
                self.datasets.append(ds)
            elif isdir(ds):
                if force_rebuild:
                    try:
                        rmtree(db_path)
                    except:
                        None
                mkdir(db_path)
                print("build index db of %s, reason %s..."%(ds, 'force rebuild' if force_rebuild else 'first time build'))
                start = time.time()
                self.dbs += self.build_db(ds, db_path)
                self.datasets.append(ds)
                print("build done in %.3f secs"%(time.time()-start))
            else:
                print("Not found dataset %s"%ds)


    def mainloop(self):
        self.doCmd('!help')
        while True:
            raw_inputs = input('> ')
            inputs = raw_inputs.strip()
            if isCmd(inputs):
                if not self.doCmd(inputs):
                    break
            else:
                self.doSearch(inputs)


    def doSearch(self, queryStr):
        if len(queryStr) > 0:
            phrase = False
            scoring = True
            if queryStr[0] in '"' and queryStr[-1] == '"':
                queryStr = queryStr[1:-1]
                phrase = True

            terms = process(queryStr)
            if not scoring:
                docs = []
                for db in self.dbs:
                    docs += db.search(terms, phrase, False)
                for doc, n in zip(sorted(docs), range(len(docs))):
                    print('%d: %s'%(n, shorter_name(doc)))
                print('total found in %d files'%len(docs))
            else:
                docs_with_tfs = []
                local_dfs = []
                total_docs = 0
                for db in self.dbs:
                    dwt, dfs = db.search(terms, phrase, True)
                    docs_with_tfs += dwt
                    local_dfs.append(dfs)
                    total_docs += db.docs_count()
                print(len(docs_with_tfs))
                global_dfs = reduce(lambda xs, ys: [x+y for x, y in zip(xs, ys)], local_dfs, [0] * len(terms))
                global_idfs = [total_docs/df for df in global_dfs]
                calc_score = lambda tfs: sum(list([x*y for x, y in zip(global_idfs, tfs)]))
                scored_docs = [(calc_score(tfs), doc) for doc, tfs in docs_with_tfs]
                scored_docs.sort(reverse=True)
                for i, s_d in enumerate(scored_docs):
                    score, doc = s_d
                    print('%d: %s -- ranking %.4f'%(i, shorter_name(doc), score))
                print('total found in %d files'%len(scored_docs))


    def doCmd(self, cmdStr):
        if cmdStr == '!exit':
            return False
        elif cmdStr == '!help':
            print(__doc__)
        elif cmdStr == '!list':
            for name  in self.datasets:
                print(name)
        elif cmdStr == '!rebuild':
            old_ds = self.datasets
            self.datasets = []
            self.dbs = []
            self.process_datasets(old_ds, True)
        elif cmdStr[:4] == '!add':
            ds = cmdStr.split()[1:]
            if ds != []:
                self.process_datasets(ds, False)
        else:
            print('unknown command ' + cmdStr)
        return True


    def build_db(self, ds_path, db_path):
        files = list_all_files(ds_path)
        l = len(files)
        filegroups = []
        s, e = 0, self.db_file_count
        while s < len(files):
            filegroups.append(files[s:min(e, l)])
            s = e
            e = e + self.db_file_count
        dbnames = ['%s/%d'%(db_path, i) for i in range(len(filegroups))]
        if self.mt_enabled:
            with Executor(max_workers=self.num_processes) as ex:
                futures = [ex.submit(DB.create, x) for x in zip(filegroups, dbnames)]
                wait(futures)
                return [f.result() for f in futures]
        else:
            return [DB.create(x) for x in zip(filegroups, dbnames)]


    def load_db(self, db_path):
        files = set([f[:-4] for f in list_all_files(db_path)])
        return [DB.load(f) for f in files]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', default='davisWiki', nargs='+', help='datasets to start with')
    parser.add_argument('-p', type=int, default=[4], nargs=1, help='number of processes to use')
    parser.add_argument('-m', type=int, default=[5000], nargs=1, help='max number of docs built into each index db')
    parser.add_argument('-f', default='no', nargs='?', help='force rebuild dataset index')
    parser.add_argument('-s', default='no', nargs='?', help='use single process')
    args = vars(parser.parse_args())
    pyse = Pyse(args['p'][0], args['m'][0], args['s'] == 'no')
    pyse.start(args['d'], args['f'] != 'no')

