### Introduction ###

a simple demo of how search engine works, it covers some very basic concepts, including tokenization, inverted index, posting intersection, positional intersection, page scoring, and disk saving.

the most interesting part of this demo would be the disk saving, as most others can be easily found through classic textbook. the idea of disk saving is an original developed idea, it works and can be scaled with certain extends, and could be further polished.

the purpose of disk saving is to resolve issue with large dataset, which cannot entirely fit into memory. the solution would be only load the postings which needed to answer the given query. 

the overall idea presented in this demo is dividing the whole dataset into different subset, and build a index db for each subset. those are reasons, it works:

1. each subset of dataset is self contained and independent, which means those individual index db can be built, load, query separately without dependency. the total query result is just a combination of each individual query result from different index db.

2. because each index db is independent, this makes execution on them perfect for parallel. in this demo it demonstrates how the built process can be paralleled. the same idea can be developed for query as well when the dataset grow bigger.

3. index db has a meta data to describe docs, tokens and their abs position in a big postings file. when query comes, it first look up the meta data to find out the position of the certain posting in big postings file, and only load those parts anwsering the query into memory.


### Uasge ###

python pyse.py -d DATASET

this will start the search engine with given DATASET loaded. If the DATASET is fresh, index db will be built at the beginning, and saved into data/

edit config/patterns.txt to config tokenization.